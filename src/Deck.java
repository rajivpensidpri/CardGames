import java.util.ArrayList;

import java.util.Random;
import java.util.Collections;


public class Deck {
	
	private ArrayList<Card> cards ; 
	
	public Deck(int npacks, int jokersPerPack) {
		cards = new ArrayList<>() ; 
		for(int i = 0 ; i < npacks ; i++) {
			 cards.addAll((new Pack(jokersPerPack)).cards) ; 
		}
		shuffle() ; 
	}
	
	
	public int getSize() {
		return cards.size() ; 
	}
	public Card drawTopCard() {
		return (cards.isEmpty())?null : cards.remove(0) ; 
	}
	
	public Card drawBottomCard() {
		return (cards.isEmpty())?null : cards.remove(cards.size()-1) ; 
		
	}
	
	public Card drawRandomCard() {
		Random r = new Random() ; 
		int idx = r.nextInt(cards.size()) ; 
		return (cards.isEmpty())?null : cards.remove(idx) ;   
	}
	
	public ArrayList<Card> drawTopKCards(int k){
		ArrayList<Card> ret = new ArrayList<>() ; 
		if (k > cards.size()) {
			return null ; 
		}
		for(int i = 0 ; i < k ; i++) {
			ret.add(cards.remove(0)); 
		}
		return ret;
	}
	
	private void shuffle() {
		Collections.shuffle(cards);
	}

	private ArrayList<ArrayList<Card> > deal(int noOfPlayers, int numOfCards) {
		shuffle();
		ArrayList<ArrayList<Card>> hands = new ArrayList<ArrayList<Card>>(noOfPlayers);

		for (int i = 0; i < numOfCards; i++) {
			for (int j = 0; j < noOfPlayers; j++) {
				ArrayList<Card> list = hands.get(j);
				list.add(cards.remove(0));
				hands.set(j, list);

			}
		}
		return hands;
	}
	private void repopulateDeck(ArrayList<Card> openedCards) {
		cards.addAll(openedCards) ;  
		
	}
	
}
